# Création de notre outils interne de gestion de projet



# Instructions d'instalation

## I- Instructions d'installation de serveur

1- Cloner le dépôt GitLab
2- Exécuter la commande suivante dans le terminal pour installer les dépendances:
    npm i

3- Démarrer le serveur Node avec la commande suivante dans le terminal:
    npm start

4- Ouvrir le lien http://localhost:3000 dans le navigateur

## II- Gestion des projets

**Cas n°1: Créer un nouveau projet**

1- Cliquer sur le bouton "Ajouter un projet"

2- Attribuer un nom au projet dans le champ "Nom du projet"

3- Attribuer une description au projet dans le champ "Description"

4- Choisir l'identifiant du client dans la liste déroulante "ID du Client"

5- Cliquer sur Enregistrer

**Cas n°2: Voir le détail d'un projet**

1- En fonction du projet que vous souhaitez regarder, cliquer sur le bouton "Voir" de la ligne correspondante

**Cas n°3: Modification un projet**

1- En fonction du projet que vous souhaitez modifier, cliquer sur le bouton "Modifier" de la ligne correspondante

2- Remplacer les données présentes dans les champs que vous souhaitez modifier

3- Cliquer sur Enregistrer

**Cas n°4: Supprimer un projet**

1- En fonction du projet que vous souhaitez supprimer, cliquer sur le bouton "Supprimer" de la ligne correspondante.

## III - Gestion des clients

**Cas n°1: Créer un nouveau client**

1- Aller sur http://localhost:3000/client-create/ pour créer un nouveau client

2- Remplissez le nom du client

3- Renseignez l'adresse du client

4- Cliquez sur Enregistrer

**Cas n°2: Voir la liste des clients**

1- Aller sur http://localhost:3000/client-all/ pour voir la liste des clients existants avec leur identifiant (le numéro)

**Cas n°3: Modifier les informations du client**

1- Aller sur http://localhost:3000/client-update/{identifiant du client} pour modifier le client en question
PS: L'identifiant du client est le numéro présent avant le nom du client sur la page qui contient la liste des clients !

2- Mettez à jour les données dans les champs "Nom" et "Adresse"

3- Cliquez sur Enregistrer

**Cas n°4: Supprimer un client**

1- Aller sur http://localhost:3000/client-delete/{identifiant du client} pour supprimer le client en question
PS: L'identifiant du client est le numéro présent avant le nom du client sur la page qui contient la liste des clients !

**Cas n°5: Voir l'information d'un client**

1- Aller sur http://localhost:3000/client-read/{identifiant du client} pour voir les informations du client en question
PS: L'identifiant du client est le numéro présent avant le nom du client sur la page qui contient la liste des clients !