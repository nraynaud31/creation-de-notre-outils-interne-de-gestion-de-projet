import { Application } from "express";
import ClientController from "./controllers/ClientController";
import ProjectController from "./controllers/ProjectController";

export default function route(app: Application)
{
    /** Static pages for Project**/
    app.get('/', (req, res) =>
    {
        ProjectController.index(req, res);
    });

    app.get('/project-all', (req, res) =>
    {
        ProjectController.index(req, res)
    });

    app.get('/project-create', (req, res) =>
    {
        ProjectController.showForm(req, res)
    });

    app.post('/project-create', (req, res) =>
    {
        ProjectController.create(req, res)
    });

    app.get('/project-read/:id_projects', (req, res) =>
    {
        ProjectController.read(req, res)
    });

    app.get('/project-update/:id_projects', (req, res) =>
    {
        ProjectController.showFormUpdate(req, res)
    });

    app.post('/project-update/:id_projects', (req, res) =>
    {
        ProjectController.update(req, res)
    });

    app.get('/project-delete/:id_projects', (req, res) =>
    {
        ProjectController.delete(req, res)
    });

    /** Static Pages for Client **/

    app.get('/client-all', (req, res) =>
    {
        ClientController.index(req, res)
    });

    app.get('/client-create', (req, res) =>
    {
        ClientController.showForm(req, res)
    });

    app.post('/client-create', (req, res) =>
    {
        ClientController.create(req, res)
    });

    app.get('/client-read/:id_client', (req, res) =>
    {
        ClientController.read(req, res)
    });

    app.get('/client-update/:id_client', (req, res) =>
    {
        ClientController.showFormUpdate(req, res)
    });

    app.post('/client-update/:id_client', (req, res) =>
    {
        ClientController.update(req, res)
    });

    app.get('/client-delete/:id_client', (req, res) =>
    {
        ClientController.delete(req, res)
    });
}
