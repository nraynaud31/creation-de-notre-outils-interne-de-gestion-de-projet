-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Nicolas RAYNAUD
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-20 09:48
-- Created:       2021-12-20 09:26
PRAGMA foreign_keys = OFF;

-- Schema: mydb
--ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "developers"(
  "id_developers" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "lastname" VARCHAR(255) NOT NULL,
  "level" VARCHAR(255) NOT NULL
);
CREATE TABLE "clients"(
  "id_client" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "address" VARCHAR(255) NOT NULL
);
CREATE TABLE "projects"(
  "id_projects" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(255) NOT NULL,
  "description" LONGTEXT NOT NULL,
  "clients_id_client" INTEGER NOT NULL,
  CONSTRAINT "fk_projects_clients"
    FOREIGN KEY("clients_id_client")
    REFERENCES "clients"("id_client")
);
CREATE INDEX "projects.fk_projects_clients_idx" ON "projects" ("clients_id_client");
CREATE TABLE "developers_has_projects"(
  "developers_id_developers" INTEGER NOT NULL,
  "projects_id_projects" INTEGER NOT NULL,
  "developers_has_projects" INTEGER PRIMARY KEY NOT NULL,
  CONSTRAINT "fk_developers_has_projects_developers1"
    FOREIGN KEY("developers_id_developers")
    REFERENCES "developers"("id_developers"),
  CONSTRAINT "fk_developers_has_projects_projects1"
    FOREIGN KEY("projects_id_projects")
    REFERENCES "projects"("id_projects")
);
CREATE INDEX "developers_has_projects.fk_developers_has_projects_projects1_idx" ON "developers_has_projects" ("projects_id_projects");
CREATE INDEX "developers_has_projects.fk_developers_has_projects_developers1_idx" ON "developers_has_projects" ("developers_id_developers");
COMMIT;
