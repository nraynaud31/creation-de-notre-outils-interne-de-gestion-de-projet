import { Request, Response } from "express-serve-static-core";

export default class ProjectController {

    /**
     * Affiche la liste des projets
     * @param req 
     * @param res 
     */
    static index(req: Request, res: Response) {
        const db = req.app.locals.db;

        const projects = db.prepare('SELECT * FROM projects').all();

        res.render('pages/index', {
            title: '',
            projects: projects
        });
    }

    /**
     * Affiche le formulaire de création du projet
     * @param req 
     * @param res 
     */
    static showForm(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const clients = db.prepare("SELECT * FROM clients").all();

        res.render('pages/project-create', {
            clients: clients
        });
    }

    /**
     * Récupère le formulaire et insere le projet en db
     * @param req 
     * @param res 
     */
    static create(req: Request, res: Response): void {
        const db = req.app.locals.db;

        db.prepare('INSERT INTO projects ("name", "description", "clients_id_client") VALUES (?, ?, ?)').run(req.body.title, req.body.content, req.body.id_client);
        console.log(req.body);

        ProjectController.index(req, res);
    }

    /**
     * Affiche 1e projet
     * @param req 
     * @param res 
     */
    static read(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const project = db.prepare('SELECT * FROM projects WHERE id_projects = ?').get(req.params.id_projects);

        res.render('pages/project-read', {
            project: project
        });
    }

    /**
     * Affiche le formulaire pour modifier un projet
     * @param req 
     * @param res 
     */
    static showFormUpdate(req: Request, res: Response) {
        const db = req.app.locals.db;

        const project = db.prepare('SELECT * FROM projects WHERE id_projects = ?').get(req.params.id_projects);
        const client = db.prepare("SELECT * FROM clients").all();
        res.render('pages/projects-update', {
            project: project,
            client: client
        });
    }

    /**
     * Récupère le formulaire du projet modifié et l'ajoute dans la db
     * @param req 
     * @param res 
     */
    static update(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('UPDATE projects SET name = ?, description = ?, clients_id_client = ? WHERE id_projects = ?').run(req.body.title, req.body.content, req.body.id_client, req.params.id_projects);

        ProjectController.index(req, res);
    }

    /**
     * Supprime un projet
     * @param req 
     * @param res 
     */
    static delete(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM projects WHERE id_projects = ?').run(req.params.id_projects);

        ProjectController.index(req, res);
    }
}