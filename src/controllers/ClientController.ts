import { Request, Response } from "express-serve-static-core";

export default class ClientController {

    /**
     * Affiche la liste des clients
     * @param req 
     * @param res 
     */
    static index(req: Request, res: Response) {
        const db = req.app.locals.db;

        const clients = db.prepare('SELECT * FROM clients').all();

        res.render('pages/client', {
            title: '',
            clients: clients
        });
    }

    /**
     * Affiche le formulaire de création du client
     * @param req 
     * @param res 
     */
    static showForm(req: Request, res: Response): void {
        res.render('pages/client-create');
    }

    /**
     * Récupère le formulaire et insère le client en db
     * @param req 
     * @param res 
     */
    static create(req: Request, res: Response): void {
        const db = req.app.locals.db;

        db.prepare('INSERT INTO clients ("name", "address") VALUES (?, ?)').run(req.body.title, req.body.content);

        ClientController.index(req, res);
    }

    /**
     * Affiche 1es clients
     * @param req 
     * @param res 
     */
    static read(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const client = db.prepare('SELECT * FROM clients WHERE id_client = ?').get(req.params.id_client);

        res.render('pages/client-read', {
            client: client
        });
    }

    /**
     * Affiche le formulaire pour modifier un client
     * @param req 
     * @param res 
     */
    static showFormUpdate(req: Request, res: Response) {
        const db = req.app.locals.db;

        const client = db.prepare('SELECT * FROM clients WHERE id_client = ?').get(req.params.id_client);

        res.render('pages/client-update', {
            client: client
        });
    }

    /**
     * Recupere le formulaire du client modifié et l'ajoute a la database
     * @param req 
     * @param res 
     */
    static update(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('UPDATE clients SET name = ?, address = ? WHERE id_client = ?').run(req.body.title, req.body.content, req.params.id_client);

        ClientController.index(req, res);
    }

    /**
     * Supprime un client
     * @param req 
     * @param res 
     */
    static delete(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM clients WHERE id_client = ?').run(req.params.id_client);

        ClientController.index(req, res);
    }
}